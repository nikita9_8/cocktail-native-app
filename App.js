/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { CocktailNavigation } from './src/Navigation/CocktailNavigation';
import { Provider } from 'react-redux';

import store from './src/store';

const App = () => {
    return (
        <Provider store={store}>
            <CocktailNavigation />
        </Provider>
    );
};

export default App;
