import React from 'react';
import {
    ActivityIndicator,
    SectionList,
    StyleSheet,
    Text,
    SafeAreaView,
    View,
} from 'react-native';
import { DrinkItem } from './DrinkItem';

export const DrinkList = ({ loading, drinks, loadMore, page, listFooter }) => {
    return (
        <>
            {loading && page === 0 ? (
                <ActivityIndicator size="large" style={styles.indicator} />
            ) : (
                <>
                    {drinks.length ? (
                        <SafeAreaView>
                            <SectionList
                                sections={drinks}
                                renderItem={({ item }) => (
                                    <DrinkItem item={item} />
                                )}
                                renderSectionHeader={({ section }) => (
                                    <Text style={styles.sectionHeader}>
                                        {section.title}
                                    </Text>
                                )}
                                keyExtractor={(item, index) => item.id}
                                onEndReached={loadMore}
                                onEndReachedThreshold={0.5}
                                ListFooterComponent={listFooter}
                                removeClippedSubviews={true}
                            />
                        </SafeAreaView>
                    ) : (
                        <View style={styles.textWrapper}>
                            <Text>No data</Text>
                        </View>
                    )}
                </>
            )}
        </>
    );
};

const styles = StyleSheet.create({
    thumb: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    sectionHeader: {
        margin: 20,
        marginBottom: 10,
        fontSize: 14,
        lineHeight: 16,
        color: '#7E7E7E',
    },
    itemTextWrapper: {
        flex: 4,
        marginLeft: 10,
        justifyContent: 'center',
    },
    listItem: {
        flex: 1,
        flexDirection: 'row',
        margin: 10,
        justifyContent: 'center',
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textWrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
