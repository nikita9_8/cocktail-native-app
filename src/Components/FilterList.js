import React from 'react';
import {
    ActivityIndicator,
    StyleSheet,
    Text,
    SafeAreaView,
    FlatList,
    TouchableOpacity,
    ScrollView,
    View,
} from 'react-native';
import { FilterItem } from './FilterItem';

export const FilterList = ({ loading, categories, pressBtn, pressFilter }) => {
    return (
        <>
            {loading ? (
                <ActivityIndicator size="large" style={styles.indicator} />
            ) : (
                <>
                    {categories.length ? (
                        <SafeAreaView>
                            <ScrollView>
                                <FlatList
                                    data={categories}
                                    renderItem={({ item }) => (
                                        <FilterItem
                                            item={item}
                                            pressFilter={pressFilter}
                                        />
                                    )}
                                    keyExtractor={(item, index) =>
                                        item.category + index
                                    }
                                />
                                <TouchableOpacity
                                    style={styles.applyBtn}
                                    onPress={pressBtn}>
                                    <Text style={styles.text}>Apply</Text>
                                </TouchableOpacity>
                            </ScrollView>
                        </SafeAreaView>
                    ) : (
                        <View style={styles.textWrapper}>
                            <Text>No data</Text>
                        </View>
                    )}
                </>
            )}
        </>
    );
};

const styles = StyleSheet.create({
    thumb: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    sectionHeader: {
        margin: 20,
        marginBottom: 10,
        fontSize: 14,
        lineHeight: 16,
        color: '#7E7E7E',
    },
    itemTextWrapper: {
        flex: 4,
        marginLeft: 10,
        justifyContent: 'center',
    },
    listItem: {
        flex: 1,
        flexDirection: 'row',
        margin: 10,
        justifyContent: 'center',
    },
    applyBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#272727',
        height: 53,
        marginHorizontal: 27,
        marginTop: 20,
    },
    text: {
        color: 'white',
        fontSize: 16,
        lineHeight: 19,
        textTransform: 'uppercase',
    },
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textWrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
