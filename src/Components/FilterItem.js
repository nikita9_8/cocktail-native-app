import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export const FilterItem = ({ item, pressFilter }) => {
    return (
        <TouchableOpacity activeOpacity={0.7} onPress={() => pressFilter(item)}>
            <View style={styles.listItem}>
                <View style={styles.itemTextWrapper}>
                    <Text
                        style={{
                            fontSize: 16,
                            lineHeight: 19,
                            color: '#7E7E7E',
                        }}>
                        {item.category}
                    </Text>
                    {item.checked && <Icon name="check" size={25} />}
                </View>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    itemTextWrapper: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        marginRight: 35,
        alignItems: 'center',
        minHeight: 40,
        justifyContent: 'space-between',
    },
    listItem: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        marginVertical: 10,
        justifyContent: 'center',
    },
});
