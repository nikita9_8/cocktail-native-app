import {
    ImageBackground,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import React from 'react';

export const DrinkItem = ({ item }) => {
    return (
        <TouchableOpacity activeOpacity={0.7}>
            <View style={styles.listItem}>
                <ImageBackground
                    source={{ uri: item.thumb }}
                    style={styles.thumb}
                />
                <View style={styles.itemTextWrapper}>
                    <Text
                        style={{
                            fontSize: 16,
                            lineHeight: 19,
                            color: '#7E7E7E',
                        }}>
                        {item.drink}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    thumb: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
        width: 100,
        height: 100,
    },
    itemTextWrapper: {
        flex: 2,
        marginLeft: 20,
        justifyContent: 'center',
    },
    listItem: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        marginVertical: 20,
        justifyContent: 'center',
    },
});
