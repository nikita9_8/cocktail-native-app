import React, { useEffect } from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import {
    loadFiltersAction,
    toggleFilter,
    loadDrinksAction,
    clearDrinkList,
} from '../store/actions/cocktailActions';
import { FilterList } from '../Components/FilterList';

export const Filters = ({ navigation }) => {
    const dispatch = useDispatch();
    const loadingFilters = useSelector((state) => state.filters.loadingFilter);
    const categories = useSelector((state) => state.filters.drinkTypes);
    const selectedTypes = useSelector((state) => state.filters.selectedTypes);

    useEffect(() => {
        dispatch(loadFiltersAction());
    }, [dispatch]);

    const applyFilterHandler = () => {
        dispatch(clearDrinkList());
        dispatch(loadDrinksAction(selectedTypes));
        navigation.goBack();
    };

    const pressFilterHandler = (item) => {
        dispatch(toggleFilter(item));
    };

    return (
        <SafeAreaView styles={styles.wrapper}>
            <FilterList
                categories={categories}
                loading={loadingFilters}
                pressBtn={applyFilterHandler}
                pressFilter={pressFilterHandler}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
});
