import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    Alert,
    StyleSheet,
    ActivityIndicator,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { loadDrinksAction } from '../store/actions/cocktailActions';
import { DrinkList } from '../Components/DrinksList';

export const Drinks = () => {
    const dispatch = useDispatch();
    const loadingDrinks = useSelector((state) => state.drinks.loadingDrinks);
    const drinks = useSelector((state) => state.drinks.drinksList);
    const selectedTypes = useSelector((state) => state.filters.selectedTypes);
    const error = useSelector((state) => state.drinks.error);
    const [pageCategory, setPageCategory] = useState(0);

    const loadMoreHandler = () => {
        const lastSelectedCategory = selectedTypes.length - 1;

        if (lastSelectedCategory > pageCategory) {
            setPageCategory((prevStatePage) => prevStatePage + 1);
        }
    };

    const loadFooter = () => {
        const lastSelectedCategory = selectedTypes.length - 1;

        if (lastSelectedCategory <= pageCategory) {
            return null;
        }

        return <ActivityIndicator size="large" />;
    };

    useEffect(() => {
        if (error) {
            Alert.alert('Warning', error, [
                {
                    text: 'OK',
                },
            ]);
        }
    }, [error, loadingDrinks]);

    useEffect(() => {
        setPageCategory(0);
    }, [selectedTypes]);

    useEffect(() => {
        dispatch(loadDrinksAction(selectedTypes, pageCategory));

        if (pageCategory === selectedTypes.length - 1) {
            Alert.alert(
                'Loading the last category of cocktails',
                '¯\\_( ͡° ͜ʖ ͡°)_/¯',
                [
                    {
                        text: 'OK',
                    },
                ]
            );
        }
    }, [pageCategory]);

    return (
        <SafeAreaView style={styles.wrapper}>
            <DrinkList
                drinks={drinks}
                loading={loadingDrinks}
                loadMore={loadMoreHandler}
                page={pageCategory}
                listFooter={loadFooter}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    },
});
