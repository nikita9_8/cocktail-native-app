import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { HeaderBackButton } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { Platform } from 'react-native';

import { Drinks } from '../Screens/Drinks';
import { Filters } from '../Screens/Filters';

const Stack = createStackNavigator();

export const CocktailNavigation = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Drinks"
                    component={Drinks}
                    options={({ navigation, route }) => ({
                        title: 'Drinks',
                        headerTitleStyle: {
                            fontSize: 24,
                            lineHeight: 28,
                        },
                        headerRight: () => (
                            <Icon.Button
                                name="filter"
                                color="black"
                                backgroundColor="white"
                                activeOpacity={1}
                                size={30}
                                onPress={() => navigation.navigate('Filters')}
                            />
                        ),
                    })}
                />
                <Stack.Screen
                    name="Filters"
                    options={({ navigation, route }) => ({
                        headerTitleStyle: {
                            fontSize: 24,
                            lineHeight: 28,
                        },
                        headerLeft: (props) => {
                            return Platform.OS === 'ios' ? (
                                <HeaderBackButton
                                    {...props}
                                    allowFontScaling={true}
                                    onPress={() => {
                                        navigation.goBack();
                                    }}
                                />
                            ) : (
                                <MaterialIcon.Button
                                    name="arrow-back"
                                    color="black"
                                    backgroundColor="white"
                                    activeOpacity={1}
                                    size={35}
                                    onPress={() => navigation.goBack()}
                                />
                            );
                        },
                    })}
                    component={Filters}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
};
