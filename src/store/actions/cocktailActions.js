import axios from 'axios';

import {
    LOAD_DRINKS,
    LOAD_FILTERS,
    APPLY_FILTERS,
    DRINKS_LOADING,
    CATEGORIES_LOADING,
    TOGGLE_FILTER,
    CLEAR_DRINK_LIST,
    DRINKS_ERROR,
} from '../types';

export const loadDrinksAction = (selectedTypes, currentPage = 0) => async (
    dispatch
) => {
    try {
        dispatch({
            type: DRINKS_LOADING,
        });

        if (selectedTypes.length) {
            const { data } = await axios.get(
                `https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=${selectedTypes[currentPage]}`
            );
            const mappedData = data.drinks.map(
                ({ idDrink, strDrink, strDrinkThumb }) => ({
                    id: idDrink,
                    drink: strDrink,
                    thumb: strDrinkThumb,
                })
            );

            const drinkList = {
                title: selectedTypes[currentPage],
                selectedTypes,
                data: mappedData,
            };

            dispatch({
                type: LOAD_DRINKS,
                payload: drinkList,
            });
        } else {
            dispatch({
                type: CLEAR_DRINK_LIST,
            });
        }
    } catch (e) {
        console.log(e);
        dispatch({
            type: DRINKS_ERROR,
            payload: e.toString(),
        });
    }
};

export const clearDrinkList = () => async (dispatch) => {
    dispatch({
        type: CLEAR_DRINK_LIST,
    });
};

export const loadFiltersAction = () => async (dispatch) => {
    try {
        const { data } = await axios.get(
            'https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list'
        );

        const mappedData = data.drinks.map(({ strCategory }) => ({
            category: strCategory,
            checked: false,
        }));

        dispatch({
            type: CATEGORIES_LOADING,
            payload: mappedData,
        });
    } catch (e) {
        console.log(e);
    }
};

export const toggleFilter = (category) => async (dispatch) => {
    dispatch({
        type: TOGGLE_FILTER,
        payload: category,
    });
};
