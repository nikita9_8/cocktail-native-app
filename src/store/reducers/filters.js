import { CATEGORIES_LOADING, LOAD_FILTERS, TOGGLE_FILTER } from '../types';

const initialState = {
    drinkTypes: [],
    selectedTypes: [],
    loadingFilter: true,
};

export const FiltersReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_FILTERS:
            return {
                ...state,
                loadingFilter: true,
            };
        case CATEGORIES_LOADING:
            const categories = [];
            action.payload.forEach((item) => {
                const index = state.drinkTypes.findIndex(
                    (i) => i.category === item.category
                );

                if (index !== -1) {
                    categories.push(state.drinkTypes[index]);
                } else {
                    categories.push(item);
                }
            });

            return {
                ...state,
                drinkTypes: categories,
                loadingFilter: false,
            };
        case TOGGLE_FILTER:
            const drinkTypes = [...state.drinkTypes];
            const selectedTypes = [...state.selectedTypes];
            const categoryIndex = drinkTypes.findIndex(
                (i) => i.category === action.payload.category
            );
            drinkTypes[categoryIndex].checked = !drinkTypes[categoryIndex]
                .checked;

            const selectedCategoryIndex = selectedTypes.indexOf(
                action.payload.category
            );

            if (selectedCategoryIndex !== -1) {
                selectedTypes.splice(selectedCategoryIndex, 1);
            } else {
                selectedTypes.push(action.payload.category);
            }

            return {
                ...state,
                drinkTypes,
                selectedTypes,
            };
        default:
            return state;
    }
};
