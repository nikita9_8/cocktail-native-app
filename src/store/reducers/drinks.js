import {
    CLEAR_DRINK_LIST,
    DRINKS_LOADING,
    LOAD_DRINKS,
    DRINKS_ERROR,
} from '../types';

const initialState = {
    drinksList: [],
    loadingDrinks: true,
    error: '',
};

export const DrinksReducer = (state = initialState, action) => {
    switch (action.type) {
        case DRINKS_LOADING:
            return {
                ...state,
                loadingDrinks: true,
                error: '',
            };
        case DRINKS_ERROR:
            return {
                ...state,
                loadingDrinks: false,
                error: action.payload,
            };
        case CLEAR_DRINK_LIST:
            return {
                ...state,
                drinksList: [],
                loadingDrinks: false,
                error: '',
            };
        case LOAD_DRINKS:
            const drinksList = state.drinksList.filter(
                (item) =>
                    action.payload.selectedTypes.find(
                        (selectedCategory) => item.title === selectedCategory
                    ) && item.title !== action.payload.title
            );

            delete action.payload.selectedTypes;

            return {
                ...state,
                drinksList: [...drinksList, { ...action.payload }],
                loadingDrinks: false,
                error: '',
            };
        default:
            return state;
    }
};
