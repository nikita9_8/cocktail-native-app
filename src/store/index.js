import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { DrinksReducer } from './reducers/drinks';
import { FiltersReducer } from './reducers/filters';

const store = combineReducers({
    drinks: DrinksReducer,
    filters: FiltersReducer,
});

export default createStore(store, applyMiddleware(thunk));
